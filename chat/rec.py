import sys, os


import pika
import time

# connection = pika.BlockingConnection(
#     pika.ConnectionParameters(host='localhost'))
# channel = connection.channel()
#
# channel.queue_declare(queue='task_queue', durable=True)
# print(' [*] Waiting for messages. To exit press CTRL+C')
#
#
# def callback(ch, method, properties, body):
#     print(" [x] Received %r" % body.decode())
#     time.sleep(body.count(b'.'))
#     print(" [x] Done")
#     ch.basic_ack(delivery_tag=method.delivery_tag)
#
#
# channel.basic_qos(prefetch_count=1)
# channel.basic_consume(queue='task_queue', on_message_callback=callback)
#
# channel.start_consuming()

def rec(x):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')

    # def callback(ch, method, properties, body):
    #     print(" [x] Received %r" % body.decode())
    #     time.sleep(body.count(b'.'))
    #     # print(" [x] Done")
    #     ch.basic_ack(delivery_tag=method.delivery_tag)
    #
    #
    # channel.basic_qos(prefetch_count=1)
    # channel.basic_consume(queue='task_queue', on_message_callback=callback)

    # Acknowledge the message
    # Get ten messages and break out
    for method_frame, properties, body in channel.consume('task_queue'):

        # Display the message parts
        # print(method_frame)
        # print(properties)
        data = body.decode()
        print('\033[91m'+'[',x,'] :','sent %r' % body.decode()+'\033[0m')

        # Acknowledge the message
        channel.basic_ack(method_frame.delivery_tag)

        # Escape out of the loop after 10 messages
        if method_frame.delivery_tag >= -1:
            # print('break')
            break

    # Cancel the consumer and return any pending messages
    requeued_messages = channel.cancel()
    print('Requeued %i messages' % requeued_messages)

    # Close the channel and the connection
    channel.stop_consuming()
    # print('stop+consuming')
    channel.close()
    channel.stop_consuming()
    connection.close()
    # try:
    #     channel.start_consuming()
    # except KeyboardInterrupt:
    #     channel.stop_consuming()
    # connection.close()
while True:
    rec('ashik')